﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EsferaAuth.Models.Users
{
    public class RegisterModel
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public DateTime Birthdate { get; set; }
        public string Cellphone { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int Iddocument { get; set; }
        public string Documentnumber { get; set; }
        public string Address { get; set; }
        public int Idcountry { get; set; }
        public int Idcivilstate { get; set; }
        public DateTime Registerdate { get; set; }
    }
}
